# LookUp
A Chrome extension to make looking up and learning Japanese vocabulary a bit easier. (Dictionary based on the dictionary used by Rikai-kun.)

You can find the extension in the Chrome webstore here: https://chrome.google.com/webstore/detail/lookup/ogpcgmfoniphkknlfmgijlkdlfaccklc

This extension was written using JavaScript, HTML, and CSS and makes use of IndexedDB for storage and Chrome's message passing for communication between the the frontend and backend.

The main features are:
- Look up words by highlighting them and then either pressing control + q or right clicking and selecting Look^Up from the context menu.
- Save words in a flashcard database and review the cards directly in your browser. The flashcard review functions can be accessed from the Look^Up icon in the Chrome toolbar.

The backend allow for consistency in the function of the Look^Up bar even when transitioning between pages.
- The frontend and backend communicate via Chrome's message passing.
- The frontend is "destroyed" every time you navigate away from a page, so its current state is save by the backend.
- When the frontend loads upon opening a new page, it requests the current state from the backend, which shares the information based on the tab ID. This way the state for each tab is saved independently.
