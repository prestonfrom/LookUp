var dictionary = [];
var textArray = [];
var theText;
var allTabs = [];
var db;
var priorityStudyList = [];

//Funciton to open the dictionary file and read it into memory.
function fileRead(url,charset) {
	var req = new XMLHttpRequest();
	req.open("GET", url, false);
	req.addEventListener('load', function(){
		theText = req.responseText;
	});
	req.send(null);
}

//Open dictionary file, convert it to an array, and parse the
//array. Open flashcard database.
fileRead("/data/dict.txt");
dictToArray();
parseArr();
//deleteDB();
setupDB();

//Each tab gets a PageObject.
//This object hold the tab ID, the search history for the tab,
//the current search word, the position in the search history,
//and the tab's display status.
function PageObject(idNum){
	this.id = idNum;
	this.searchHistory = [];
	this.searchWord = [];
	this.currentPositionInHistory = 0;
	this.displayStatus = false;	
}

//Listener to process messages from view/flashcard scripts.
//Formatting should be: 
//{sender: xxx, recipient: "background", msg : xxx, content: xxx | [xxx]}
chrome.runtime.onMessage.addListener(function(req, sender, resSend){
	if(req["sender"] == "Flashcards" && req["recipient"] == "background"){
		if(req["msg"] == "GetFlashcard"){
			getFlashcard(req["content"], resSend);
			return true;
		}
		if(req["msg"] == "GetAllFlashcards"){
			getAllFlashcards(resSend);
			return true;
		}
		else if(req["msg"] == "UpdateFlashcard"){
			//alert(JSON.stringify(req["content"]));
			updateFlashcard(req["content"], resSend);
			return true;
		}
		else if(req["msg"] == "DeleteFlashcard"){
			//alert(JSON.stringify(req["content"]));
			deleteFlashcard(req["content"], resSend);
			return true;
		}
		else if(req["msg"] == "AddToPriorityStudyList"){
			addToPriorityStudyList(req["content"], resSend);
			return true;
		}
		else if(req["msg"] == "GetFlashcardsForReview"){
			getFlashcardsForReview(resSend);
			return true;
		}
		else if(req["msg"] == "ClearPriorityList"){
			priorityStudyList.length = 0;
		}
	}
	if(req["sender"] == "LookUpBar" && req["recipient"] == "background"){
		var curTab = getCurTab(sender.tab["id"]);
				
		if(req["msg"] == "Word"){
			search(req["content"], curTab, resSend);
		}
		else if(req["msg"] == "AddFlashcard"){
			newFlashcard(req["content"], resSend);
			return true;
		}
		else if(req["msg"] == "Prev"){
			getPrev(curTab,resSend);
		}
		else if(req["msg"] == "Next"){
			getNext(curTab,resSend);
		}
		else if(req["msg"] == "DisplayStatus" && req["content"] == "Get"){
			getDisplayStatus(curTab,resSend);
		}
		else if(req["msg"] == "DisplayStatus" && req["content"][0] == "Set"){
			setDisplayStatus(req["content"][1], curTab, resSend)
		}
		else {
			alert("Sorry, there was an error with " + req["msg"])
		}
	}
});

//Function to add a saved flashcard to the priority study list
function addToPriorityStudyList(flashcard){
	if(inPriorityStudyList(flashcard)){
		alert("Already added to priority study list.");
		return;
	}
	else{
		priorityStudyList.push(flashcard);
		alert("Added " + flashcard.word + " to priority study list");
		return;
	}
}

//Checks if word has been added yet or not, and returns true (if present)
// or false (if absent).
function inPriorityStudyList(card){
	for(prop in priorityStudyList){
		if(priorityStudyList[prop].word == card.word){
			if(priorityStudyList[prop].reading == card.reading){
				if(priorityStudyList[prop].definition == card.definition){
					return true;
				}
			}
		}
	}
	return false;
}

//Gets all flashcards stored in indexeddb database
//and returns them using callback function.
function getFlashcardsForReview(callback){
	var allFlashcardsForReview = {
		priorityCards : priorityStudyList,
		remainingCards : []
	};
	
	var transaction = db.transaction("SaveFlashcards", "readwrite");
	var objectStore = transaction.objectStore("SaveFlashcards");
	var message = {"sender" : "background", "recipient": "LookUpBar", "empty" : true};
	
	var request = objectStore.openCursor();
	
	request.onsuccess = function(evt){
		var cursor = evt.target.result;
		if(cursor){
			if(!inPriorityStudyList(cursor.value)){
				allFlashcardsForReview.remainingCards.push(cursor.value);
			}
			cursor.continue();
		}
	}

	transaction.oncomplete = function(){
		//console.log(JSON.stringify(allFlashcardsForReview));
		message.content = allFlashcardsForReview;
		message.empty = false;
		callback(message);
	}
}

//Get the array index of the current tab or create a new
//page object and append it to array and return index if
//the current tab does not yet have a page object.
function getCurTab(id){
	for(var i = 0; i < allTabs.length; i++){
		if(allTabs[i].id == id){
			return i;
		}
	}
	var newTab = new PageObject(id);
	allTabs.push(newTab);
	return allTabs.length - 1;
}

//Set the display status of the current tab (called
//after showing/hiding the look up bar with the view script)
function setDisplayStatus(dispStat, tab, callback){
	allTabs[tab]["displayStatus"] = dispStat;
	var returnMessage = {"sender" : "background", "recipient": "LookUpBar", "msg" : "ReturnDisplayStatus", "DisplayStatus" : allTabs[tab]["displayStatus"]};
	callback(returnMessage);
}

//Returns the current display status for the current tab as well as
//the current search results for the tab. (So users can look up a word,
//click a link, and still have that word on screen.)
function getDisplayStatus(tab, callback){
	var empt = false;
	if(allTabs[tab].searchHistory.length < 1) empt = true;
	var returnMessage = {"sender" : "background", "recipient": "LookUpBar", "msg" : "ReturnDisplayStatus", "DisplayStatus" : allTabs[tab]["displayStatus"], "empty" : empt, "lookUpResults" : allTabs[tab].searchHistory[allTabs[tab].currentPositionInHistory], "SearchWord": allTabs[tab].searchWord[allTabs[tab].currentPositionInHistory]};
	callback(returnMessage);
}

//Return the previous search result from the current tab relative to its
//current position in search history.
//If no more previous search histories, returns empty as true and indicates
//the direction (previous).
function getPrev(tab, callback){
	if(allTabs[tab].currentPositionInHistory > 0){
		allTabs[tab].currentPositionInHistory--;
		var returnMessage = {"sender" : "background", "recipient": "LookUpBar", "empty" : false, "History": allTabs[tab].searchHistory[allTabs[tab].currentPositionInHistory], "SearchWord": allTabs[tab].searchWord[allTabs[tab].currentPositionInHistory]};
	} else {
		var returnMessage = {"sender" : "background", "recipient": "LookUpBar", "empty" : true, "direction" : "Prev"};
	}
	callback (returnMessage);
}

//Same as above, but for next item in search history.
//Next item in search history is returned relative to current position.
//Retruns empty as true if no more items.
function getNext(tab, callback){
	if(allTabs[tab].currentPositionInHistory < allTabs[tab].searchHistory.length - 1){
		allTabs[tab].currentPositionInHistory++;
		var returnMessage = {"sender" : "background", "recipient": "LookUpBar", "empty" : false,  "History": allTabs[tab].searchHistory[allTabs[tab].currentPositionInHistory], "SearchWord": allTabs[tab].searchWord[allTabs[tab].currentPositionInHistory]};
	} else {
		var returnMessage = {"sender" : "background", "recipient": "LookUpBar", "empty" : true, "direction" : "Next"};
	}
	callback(returnMessage);
}

//Loops through the array containing the dictionary.
//If the first part of a word matches the search term, it is added to 
//an array and returned.
//Search term and results are also added to the search history.
function search(searchWord, tab, callback){
	var len = searchWord.length;
	var resultsArray = [];
	for(prop in dictionary){
		if(searchWord == dictionary[prop]["word"].slice(0,len))
			resultsArray.push(dictionary[prop]);
	}
	allTabs[tab].searchHistory.push(resultsArray);
	allTabs[tab].searchWord.push(searchWord);
	allTabs[tab].currentPositionInHistory = allTabs[tab].searchHistory.length - 1;
	if(resultsArray.length > 0)
		var returnMessage = {"sender" : "background", "recipient": "LookUpBar", "empty" : false, "lookUpResults" : resultsArray};
	else
		var returnMessage = {"sender" : "background", "recipient": "LookUpBar", "empty" : true, "lookUpResults" : resultsArray};

	callback(returnMessage)
}

//Parses the dictionary text saved as an array 
//and puts it together as a JSON object
//then adds the objects to the dictionary array
function parseArr(){
	for(var i = 0; i < textArray.length;i++){
		var newEntry = textArray[i];
		var endPos = newEntry.indexOf(" ");
		var word = newEntry.substr(0, endPos);
		var reading;
		var startPos;
		if(newEntry[endPos+1] == "["){
			newEntry = newEntry.substr(endPos+2);
			endPos = newEntry.indexOf("]");
			reading = newEntry.substr(0, endPos);
			startPos = newEntry.indexOf("/");
		}
		else{
			reading = word;
			var katakana;
			var hiragana = "";
			for(var x = 0; x < reading.length; x++){
				katakana = reading.charCodeAt(x);
				if(katakana >= 12449 && katakana <= 12538){
					hiragana += String.fromCharCode(katakana - 96);
				} else{
					hiragana += reading[x];
				}
			}
			reading = hiragana;
			startPos = newEntry.indexOf("/");
		}
		var def = newEntry.substr(startPos+1);
		var arrOb = { "word": word, "reading": reading, "definition": def};
		dictionary.push(arrOb);
	}
}

//Splits the text in dictionary file into an array
function dictToArray(){
	var go = 1;
	var startPos = 0;
	var endPos = 0;
	
	while(theText.length>5){
		endPos = theText.indexOf("\n");
		newString = theText.substr(0, endPos);
		theText = theText.substr(endPos+1);
		textArray.push(newString);
	}
}

//Opens database when Chrome starts.
//Runs onupgradeneeded when first installed.
function setupDB(){
	var req = window.indexedDB.open("LookUpFC", 1);
	req.onerror = function(evt){
		alert("Failed to setup/connect to database");
	}
	
	req.onsuccess = function(evt){
		//alert("Connected to database.");
		db = evt.target.result;
	}
	
	req.onupgradeneeded = function(evt){
		var newDB = evt.currentTarget.result;
		var objectStore = newDB.createObjectStore("SaveFlashcards", {keyPath:"id", autoIncrement:true});
		objectStore.createIndex("word", "word", {unique: false});
		objectStore.createIndex("reading", "reading", {unique: false});
		objectStore.createIndex("definition", "definition", {unique: false});
		objectStore.createIndex("right", "right", {unique: false});
		objectStore.createIndex("wrong", "wrong", {unique: false});
		objectStore.createIndex("lastStudied", "lastStudied", {unique: false});
		alert("Updated database");
	};
}

//Function to delete a database. Essentially cannot be
//accessed by user.
function deleteDB(){
	var req = window.indexedDB.deleteDatabase("LookUpFC");
	req.onsuccess = function(evt){
		alert("Deleted DB");
	}
	req.onerror = function(evt){
		alert("Couldn't delete DB");
	}
}

//Checks if a word has been saved a flashcard yet or not.
//Displays error message if already in database and exits via return statement.
//If not in database, the callback function is called with the word.
function checkInDB(checkWord, callback){
	var transaction = db.transaction("SaveFlashcards", "readwrite");
	var objectStore = transaction.objectStore("SaveFlashcards");
	var request = objectStore.openCursor();
	request.onsuccess = function(evt){
		var cursor = evt.target.result;
		if(cursor){
			if(cursor.value.word == checkWord.word){
				if(cursor.value.reading == checkWord.reading){
					if(cursor.value.definition == checkWord.definition){
						alert(checkWord["word"] + " already in database.");
						return;
					}
				}
			}
			cursor.continue();
		} else {
			callback(checkWord);
		}
	}
	request.onerror = function(evt){
		alert("Couldn't check.");
	}
}

//Add a new flashcard to the flashcard database
//In addition to definition, reading, and word, 
//each entry also store the # of times the flashcard
//was reviewed correctly/incorrectly and when it was last reviewed.
var addNewFlashcard = function(newWord){
	var theword = newWord["word"];
	
	var transaction = db.transaction("SaveFlashcards", "readwrite");
	var objectStore = transaction.objectStore("SaveFlashcards");
	
	var date = new Date();
	
	var curDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
	
	var payload = {"word" : newWord["word"], "reading" : newWord["reading"], "definition":newWord["definition"], "right" : 0, "wrong" : 0, "lastStudied":curDate};
	var request = objectStore.add(payload);
	request.onsuccess = function(evt){
		alert(newWord["word"] + " has been added to your flashcards.");
	}
	request.onerror = function(evt){
		alert("Sorry, there was a problem adding " + newWord["word"] + " to your flashcards.");
	}
}

//Updates flashcards -- called after a flashcard
//has been reviewed and changes the right/wrong number
//and date of review.
function updateFlashcard(updateWord, callback){
	//alert(JSON.stringify("updateWord"));
	var transaction = db.transaction("SaveFlashcards", "readwrite");
	var objectStore = transaction.objectStore("SaveFlashcards");
	
	var date = new Date();
	
	var curDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
	
	var idNum = updateWord["id"];
	//alert(typeof idNum);
	
	var payload = {"id" : idNum, "word" : updateWord["word"], "reading" : updateWord["reading"], "definition":updateWord["definition"], "right" : updateWord["right"], "wrong" : updateWord["wrong"], "lastStudied":curDate};
	var request = objectStore.put(payload);
	request.onsuccess = function(evt){
		//alert(updateWord["word"] + " has been updated");
	};
	request.onerror = function(evt){
		//console.log("Could not update " + updateWord["word"]);
	};
}

//Intermediary function that calls checkInDB with addNewFlashcard
//as the callback function
function newFlashcard(newWord){
	checkInDB(newWord, addNewFlashcard);
}

//Searches the database for a specific saved word.
//Creates return message JSON object which has empty
//intially set to true.
//If the word is found, empty is set to false and the word
//is appended to the object. JSON object is returned via
//callback function.
function getFlashcard(getWord, callback){
	var transaction = db.transaction("SaveFlashcards", "readwrite");
	var objectStore = transaction.objectStore("SaveFlashcards");
	//console.log(JSON.stringify(getWord));
	var message = {"sender" : "background", "recipient": "LookUpBar", "empty" : true};
	
	var request = objectStore.openCursor();
	
	transaction.oncomplete = function(){
		callback(message);
	}
	
	request.onsuccess = function(evt){
		var cursor = evt.target.result;
		if(cursor){
			//console.log(cursor.value);
			if(cursor.value.word == getWord.word){
				if(cursor.value.reading == getWord.reading){
					if(cursor.value.definition == getWord.definition){
						var returnWord = cursor.value
						message.GetFromDB = returnWord;
						message.empty = false;
					}
				}
			}
			cursor.continue();
		}
	}
}

//As the name implies, this function gets all the flashcards in 
//the database and returns them in one big JSON object.
//Note that if the database is empty, the 
function getAllFlashcards(callback){
	var transaction = db.transaction("SaveFlashcards", "readwrite");
	var objectStore = transaction.objectStore("SaveFlashcards");
	var message = {"sender" : "background", "recipient": "LookUpBar", "empty" : true};
	
	var request = objectStore.openCursor();
	
	var allFlashcards = [];
	
	transaction.oncomplete = function(){
		if(allFlashcards.length > 0){
			message.content = allFlashcards;
			message.empty = false;
		}
		callback(message);
	}
	
	request.onsuccess = function(evt){
		var cursor = evt.target.result;
		if(cursor){
			allFlashcards.push(cursor.value);
			cursor.continue();
		}
	}
}

//Deletes a flashcard from the database.
function deleteFlashcard(cardId, callback){
	var transaction = db.transaction("SaveFlashcards", "readwrite");
	var objectStore = transaction.objectStore("SaveFlashcards");
	var message = {"sender" : "background", "recipient": "LookUpBar", "empty" : true};
	
	var request = objectStore.delete(cardId);
	
	request.onsuccess = function(evt){
		var returnMessage = {"sender" : "background", "recipient": "LookUpBar", "msg" : "FlashcardDeleted", "DeletedCardNumber" : cardId};
		callback(returnMessage);
	}
}

var getSelectedText = function(){
	var message = {"sender" : "background", "recipient": "LookUpBar", "msg" : "LookupSelectedText"};
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
		chrome.tabs.sendMessage(tabs[0].id, message, function(){});
	});
};

//This adds the look up option to the right click menu.
//Reference: http://stackoverflow.com/questions/5193350/chrome-extension-append-functions-to-right-click-menu
chrome.contextMenus.create({
	title: "Look^Up",
	contexts:["selection"], 
	onclick: getSelectedText
});
