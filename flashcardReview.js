var wrongcards = [];
var updatingcard;
var priorityStudyList = [];
var finishedFlashcards = {
	right : [],
	wrong : []
};
var count = 0;
var numAnswered = 0;

//Used for debugging
var printToConsole = function(somethingToBePrinted){
	console.log(somethingToBePrinted);
}

//used for debugging
var printStringify = function(somethingToBePrinted){
	console.log(JSON.stringify(somethingToBePrinted));
}

//Used to pass messages and callback functions
//to the background
var passMessage = function(message, callback){
	chrome.runtime.sendMessage(message, callback);
}

//Used to prepare JSON message object
var prepMessage = function(msg, content){
	if(content == undefined) content = "";
	var message = {
		"sender" : "Flashcards",
		"recipient" : "background",
		"msg" : msg,
		"content" : content
	}
	
	return message;
}

//Create answer button, add event listener depending on if it is
//the correct answer button or not, and return the button
function createButton(buttonText, correctAnswer, flashcard, flashcards, callback){
	var newButton = document.createElement("button");
	if(buttonText.length > 120)
		newButton.innerHTML = buttonText.substr(0, 130) + "...";
	else
		newButton.innerHTML = buttonText;

	if(correctAnswer) {
		newButton.className = "AnswerButtonRight";
		newButton.addEventListener("click", rightAnswer.bind(newButton, flashcard, flashcards, callback));
	}
	if(!correctAnswer) {
		newButton.className = "AnswerButtonWrong";
		newButton.addEventListener("click", wrongAnswer.bind(newButton, flashcard, flashcards, callback));
	}
	return newButton;
	
}

//Function called when a correct-answer button is clicked
//Background flashes green if this button is clicked and
//all cards in the row are then disabled.
//A message is passed to the background to update the right answer
//counter for the corresponding flashcard.
function rightAnswer(flashcard, flashcards, callback){
	document.getElementById("FCDisplayDiv").style.backgroundColor = "green";
	setTimeout(function(){document.getElementById("FCDisplayDiv").style.backgroundColor = "white"}, 500);
	flashcard.right++;
	finishedFlashcards.right.push(flashcard);
	var message = prepMessage("UpdateFlashcard", flashcard);
	passMessage(message);
	numAnswered++;
	this.parentNode.style.opacity = 0.5;
	var nodes = this.parentNode.childNodes;
	for(var i = 0; i < nodes.length; i++){
		var newChild = nodes[i].cloneNode(true);
		nodes[i].parentNode.replaceChild(newChild, nodes[i]);
	}
	if(numAnswered == 2){
		count++;
		numAnswered = 0;
		setTimeout(function(){
			var continueButton = document.createElement("button");
			continueButton.innerHTML = "Continue to next card.";
			continueButton.addEventListener("click", function(){
				callback(flashcards);
			});
			continueButton.className = "AnswerButtonRight";
			continueButton.style.marginLeft = "calc(50% - 100px)";
			document.getElementById("FCDisplayDiv").appendChild(continueButton);
		}, 500);

	}
}

//Function called when a incorrect-answer button is clicked
//Background flashes red if this button is clicked and
//all cards in the row are then disabled.
//A message is passed to the background to update the wrong answer
//counter for the corresponding flashcard.
function wrongAnswer(flashcard, flashcards, callback){
	document.getElementById("FCDisplayDiv").style.backgroundColor = "red";
	setTimeout(function(){document.getElementById("FCDisplayDiv").style.backgroundColor = "white"}, 500);
	flashcard.wrong++;
	finishedFlashcards.wrong.push(flashcard);
	var message = prepMessage("UpdateFlashcard", flashcard);
	passMessage(message);
	numAnswered++;
	this.parentNode.style.opacity = 0.5;
	var nodes = this.parentNode.childNodes;
	for(var i = 0; i < nodes.length; i++){
		var newChild = nodes[i].cloneNode(true);
		nodes[i].parentNode.replaceChild(newChild, nodes[i]);
	}
	if(numAnswered == 2){
		count++;
		numAnswered = 0;
		setTimeout(function(){
			var continueButton = document.createElement("button");
			continueButton.innerHTML = "Continue to next card.";
			continueButton.addEventListener("click", function(){
				callback(flashcards);
			});
			continueButton.className = "AnswerButtonRight";
			continueButton.style.marginLeft = "calc(50% - 100px)";
			document.getElementById("FCDisplayDiv").appendChild(continueButton);
		}, 500);
		
	}
}

//Load a flashcard and display it for study
//Also loads incorrect-answer buttons using other flashcards
function loadFlashcard(flashcard, flashcards, callback){
	var FCDisplayDiv = document.getElementById("FCDisplayDiv");
	FCDisplayDiv.innerHTML = "";
	
	var wordDiv = document.createElement("div");
	wordDiv.appendChild(document.createTextNode(flashcard.word));
	wordDiv.className = "WordDiv";
	
	var readingDiv = document.createElement("div");
	readingDiv.className = "ReadingDiv";
	var correctAnswer = Math.floor(((Math.random() * 10) % 4));
	var wrongReadings = [];
	for(var i = 0; i < 4; i++){
		if(i == correctAnswer){
			var newButton = createButton(flashcard["reading"], true, flashcard, flashcards, callback);
		} else {
			var wrongReading = flashcard.reading;
			while(wrongReading == flashcard.reading){
				wrongReading = flashcards[Math.floor(((Math.random() * 10) % flashcards.length))].reading;
				for(prop in wrongReadings){
					if(wrongReadings[prop] == wrongReading){
						wrongReading = flashcard.reading;
					}
				}
			}
			wrongReadings.push(wrongReading);
			var newButton = createButton(wrongReading, false, flashcard, flashcards, callback);
		}
		readingDiv.appendChild(newButton);
		var nodes = readingDiv.childNodes;
	}
	
	var definitionDiv = document.createElement("div");
	definitionDiv.className = "DefinitionDiv";
	var correctAnswer = Math.floor(((Math.random() * 10) % 4));
	var wrongDefs = [];
	for(var i = 0; i < 4; i++){
		if(i == correctAnswer){
			var newButton = createButton(flashcard.definition, true, flashcard, flashcards, callback);
		} else {
			var wrongDefinition = flashcard.definition;
			while(wrongDefinition == flashcard.definition){
				wrongDefinition = flashcards[Math.floor(((Math.random() * 10) % flashcards.length))].definition;
				for(prop in wrongDefs){
					if(wrongDefs[prop] == wrongDefinition){
						wrongDefinition = flashcard.definition;
					}
				}
			}
			wrongDefs.push(wrongDefinition);
			var newButton = createButton(wrongDefinition, false, flashcard, flashcards, callback);
		}
		definitionDiv.appendChild(newButton);
	}
	FCDisplayDiv.appendChild(wordDiv);
	FCDisplayDiv.appendChild(document.createElement("br"));
	FCDisplayDiv.appendChild(readingDiv);
	FCDisplayDiv.appendChild(document.createElement("br"));
	FCDisplayDiv.appendChild(definitionDiv);
	FCDisplayDiv.appendChild(document.createElement("br"));
}

//This is the "loop" function for flashcard study.
//Loads the next flashcards to be studied until no more cards to be studied.
//When finished, statistics on user's study session are displayed.
function studyFlashcards(flashcards){
	if(count < flashcards.length){
		loadFlashcard(flashcards[count], flashcards, studyFlashcards);
	} else {
		document.getElementById("FCDisplayDiv").innerHTML = "";
		var resultsMessage = document.createElement("div");
		resultsMessage.className = "MessageDiv";
		resultsMessage.appendChild(document.createTextNode("Results"));
		resultsMessage.appendChild(document.createElement("br"));
		resultsMessage.appendChild(document.createTextNode("Correct answers: " + finishedFlashcards.right.length));
		resultsMessage.appendChild(document.createElement("br"));
		resultsMessage.appendChild(document.createTextNode("Incorrect answers: " + finishedFlashcards.wrong.length));
		resultsMessage.appendChild(document.createElement("br"));
		document.getElementById("FCDisplayDiv").appendChild(resultsMessage);

		if(finishedFlashcards.wrong.length > 0 ){
			count = 0;
			var continueButton = document.createElement("button");
			continueButton.innerHTML = "Study cards you got wrong.";
			wrongcards = returnWrongCards();
			finishedFlashcards.right.length = 0;
			finishedFlashcards.wrong.length = 0;
			continueButton.addEventListener("click", function(){
				studyWrongCards(flashcards);
			});
			continueButton.className = "AnswerButtonRight";
			continueButton.style.marginLeft = "10px";
			document.getElementById("FCDisplayDiv").appendChild(continueButton);
		} else {
			var continueButton = document.createElement("button");
			continueButton.innerHTML = "No wrong answers! Good job!<br>Click to close.";
			continueButton.addEventListener("click", function(){
				window.close();
			});
			continueButton.className = "AnswerButtonRight";
			continueButton.style.marginLeft = "10px";
			document.getElementById("FCDisplayDiv").appendChild(continueButton);
		}
	
	}
}

//Checks if a card has been added to the wrong cards
//yet or not and returns true if already in array
//and false if not
function inWrongCards(word, wrongcards){
	for(var i = 0; i < wrongcards.length; i++){
		if(word.id == wrongcards[i].id){
			return true;
		}
	}
	return false;
}

//Funcion to return an array of all the wrong flashcards
//so the wrong/right cards array can be cleared
function returnWrongCards(){
	var wrongcards = [];
	var allWrongCards = finishedFlashcards.wrong;
	for(prop in allWrongCards){
		if(!inWrongCards(allWrongCards[prop], wrongcards))
			wrongcards.push(allWrongCards[prop]);
	}
	return wrongcards;
}

//Function to study wrong cards after finishing
//first round of studying
function studyWrongCards(flashcards){
	if(count < wrongcards.length){
		loadFlashcard(wrongcards[count], flashcards, studyWrongCards);
	} else {
		document.getElementById("FCDisplayDiv").innerHTML = "";
		var resultsMessage = document.createElement("div");
		resultsMessage.className = "MessageDiv";
		resultsMessage.appendChild(document.createTextNode("Results"));
		resultsMessage.appendChild(document.createElement("br"));
		resultsMessage.appendChild(document.createTextNode("Correct answers: " + finishedFlashcards.right.length));
		resultsMessage.appendChild(document.createElement("br"));
		resultsMessage.appendChild(document.createTextNode("Incorrect answers: " + finishedFlashcards.wrong.length));
		resultsMessage.appendChild(document.createElement("br"));
		document.getElementById("FCDisplayDiv").appendChild(resultsMessage);

		if(finishedFlashcards.wrong.length > 0 ){
			count = 0;
			var continueButton = document.createElement("button");
			continueButton.innerHTML = "Study cards you got wrong.";
			wrongcards = returnWrongCards();
			finishedFlashcards.right.length = 0;
			finishedFlashcards.wrong.length = 0;
			continueButton.addEventListener("click", function(){
				studyWrongCards(flashcards);
			});
			continueButton.className = "AnswerButtonRight";
			continueButton.style.marginLeft = "10px";
			document.getElementById("FCDisplayDiv").appendChild(continueButton);
		} else {
			var continueButton = document.createElement("button");
			continueButton.innerHTML = "No wrong answers! Good job!<br>Click to close.";
			continueButton.addEventListener("click", function(){
				window.close();
			});
			continueButton.className = "AnswerButtonRight";
			continueButton.style.marginLeft = "10px";
			document.getElementById("FCDisplayDiv").appendChild(continueButton);
		}
	}
}

//We need four flashcards minimum for the study functions to work.
//If four cards have not been saved, the user is prompted to find more words to save
function tooFewFlashcards(len){
	var needsXCards = 4 - len;
	var resultsMessage = document.createElement("div");
	resultsMessage.className = "MessageDiv";
	resultsMessage.appendChild(document.createTextNode("Sorry, you must have 4 or more flashcards save before you can start reviewing. Please add " + needsXCards + " or more cards before reviewing!"));
	document.getElementById("FCDisplayDiv").appendChild(resultsMessage);
}

//Received response from request to background to send all cards
//for studying. Processes the response by sorting the priority
//cards and remaining cards according to their ratio of right to wrong.
//Priority list is cleared each time cards are studied.
function setupFlashcardStudy(response){
	var flashcards = response["content"];
	var len = flashcards.priorityCards.length + flashcards.remainingCards.length;
	if(len < 4){
		tooFewFlashcards(len);
		return;
	}
	flashcards.priorityCards.sort(function(a, b){
		var aRatio = a.right / (a.right + a.wrong + 1);
		var bRatio = b.right / (b.right + b.wrong + 1);
		if(aRatio < bRatio || a.right + a.wrong == 0)
			return -1
		else 
			return 1;
	});
	flashcards.remainingCards.sort(function(a, b){
		var aRatio = a.right / (a.right + a.wrong);
		var bRatio = b.right / (b.right + b.wrong);
		if(aRatio < bRatio)
			return -1
		else 
			return 1;
	});
	var allFlashcards = [];
	for(prop in flashcards.priorityCards){
		allFlashcards.push(flashcards.priorityCards[prop]);
	}
	for(prop in flashcards.remainingCards){
		allFlashcards.push(flashcards.remainingCards[prop]);
	}
	
	var message = prepMessage("ClearPriorityList");
	passMessage(message);
	
	studyFlashcards(allFlashcards);
}

//Starter function for studying flashcards
//Sends request to background and passes response to setup function
function startFlashcardStudy(){
	var FCDisplayDiv = document.getElementById("FCDisplayDiv");
	FCDisplayDiv.innerHTML = "";
	
	count = 0;
	
	finishedFlashcards.right.length = 0;
	finishedFlashcards.wrong.length = 0;
	
	var message = prepMessage("GetFlashcardsForReview");
	passMessage(message, setupFlashcardStudy);
}

startFlashcardStudy();
