var updatingcard;

var printToConsole = function(somethingToBePrinted){
	//console.log(somethingToBePrinted);
}

var printStringify = function(somethingToBePrinted){
	//console.log(JSON.stringify(somethingToBePrinted));
}

//Used to send messages to and handle responses from
//background
var passMessage = function(message, callback){
	chrome.runtime.sendMessage(message, callback);
}

//Prepares message JSON objects and returns objects
var prepMessage = function(msg, content){
	if(content == undefined) content = "";
	var message = {
		"sender" : "Flashcards",
		"recipient" : "background",
		"msg" : msg,
		"content" : content
	}
	
	return message;
}

//Sends message to background to delete a specific flashcard
//from the database along with getAllFlashcards as the
//callback function. (This way, flashcards are reloaded every
//time a card is deleted to avoid stale data.)
function deleteFlashcard(cardId){
	var message = prepMessage("DeleteFlashcard", cardId);
	passMessage(message, getAllFlashcards);
}

//Gets a specific flashcard from the database.
//Mostly used for debugging
var getFlashcard = function(word){
	var message = prepMessage("GetFlashcard", getWord);
	printStringify(message);
	passMessage(message, printToConsole);
}

//Gets all flashcards from the database by sending
//request to background with a callback to 
//showAllFlashcards, which displays flashcards.
var getAllFlashcards = function(){
	var searchDiv = document.getElementById("SearchDiv");
	if(searchDiv != null) searchDiv.innerHTML = "";
	var message = prepMessage("GetAllFlashcards");
	passMessage(message, showAllFlashcards);
}

//Creates a delete button with the id for a specific
//flashcard and returns the button
function createDeleteButton(idNum){
	var deleteButton = document.createElement("input");
	deleteButton.type = "button";
	deleteButton.value = "Delete Flashcard";
	deleteButton.addEventListener("click", function(){
		deleteFlashcard(idNum);
	});
	deleteButton.className = "CardInfoSpan";
	return deleteButton;
}

//Sends a message to background to add a specific
//card to the priority study list
function addFlashcardToPriorityStudy(flashcard){
	var message = prepMessage("AddToPriorityStudyList", flashcard);
	passMessage(message, printStringify);
}

//Creates a button to add a flashcard to the priority
//study list and returns it
function createPriorityStudyButton(flashcard){
	var priorityButton = document.createElement("input");
	priorityButton.type = "button";
	priorityButton.value = "Add Flashcard to priority study list";
	priorityButton.addEventListener("click", function(){
		addFlashcardToPriorityStudy(flashcard);
	});
	return priorityButton;
}

//Helper function used by showAllFlashcards to create 
//divs for displaying save flashcards. Returns div whend done.
function setupFlashcardEntry(flashcard){
	
	var newDiv = document.createElement("div");
	newDiv.className = "BrowseCards";
	
	var wordSpan = document.createElement("span");
	wordSpan.appendChild(document.createTextNode(flashcard.word));
	wordSpan.className = "WordSpan";
	
	var readingSpan = document.createElement("span");
	readingSpan.appendChild(document.createTextNode(flashcard.reading));
	
	var definitionSpan = document.createElement("span");
	definitionSpan.appendChild(document.createTextNode(flashcard.definition));
	definitionSpan.className = "DefSpan";
	
	var rightSpan = document.createElement("span");
	rightSpan.appendChild(document.createTextNode("Times answered correctly: " + flashcard.right));
	rightSpan.className = "CardInfoSpan";
	
	var wrongSpan = document.createElement("span");
	wrongSpan.appendChild(document.createTextNode("Times answered incorrectly: " + flashcard.wrong));
	wrongSpan.className = "CardInfoSpan";
	
	newDiv.appendChild(wordSpan);
	newDiv.appendChild(readingSpan);
	newDiv.appendChild(document.createElement("br"));
	newDiv.appendChild(definitionSpan);
	newDiv.appendChild(document.createElement("br"));
	newDiv.appendChild(rightSpan);
	newDiv.appendChild(wrongSpan);
	newDiv.appendChild(createDeleteButton(flashcard.id));
	newDiv.appendChild(createPriorityStudyButton(flashcard));
	
	return newDiv;
}

//Displays all flashcards saved in database or an error message
//if there are no saved flashcards.
function showAllFlashcards(flashcards){
	if(!flashcards.empty){
		var displayDiv = document.getElementById("FCDisplayDiv");
		displayDiv.innerHTML = "";
		flashcardArray = flashcards.content;
		for(prop in flashcardArray){
			//consoleconsole.log(JSON.stringify(flashcardArray[prop]));
			var newDiv = setupFlashcardEntry(flashcardArray[prop]);
			displayDiv.appendChild(newDiv);
		}
	}
	else{
		var displayDiv = document.getElementById("FCDisplayDiv");
		displayDiv.innerHTML = "";
		var errorDiv = document.createElement("div");
		errorDiv.className = "MessageDiv";
		errorDiv.appendChild(document.createTextNode("You haven't saved any flashcards yet."));
		displayDiv.appendChild(errorDiv);

	}
}

//Displays a search box and allows users to search saved
//flashcards. Requires a 100% match to return cards.
//Uses setupSearchForFlashcard as helper function to
//prepare search box.
function searchForFlashcard(searchWord, response){
	var flashcards = response["content"];
	var displayDiv = document.getElementById("FCDisplayDiv");
	displayDiv.innerHTML = "";
	for(prop in flashcards){
		if(flashcards[prop].word == searchWord){
			//console.log(JSON.stringify(flashcards[prop]));
			var newDiv = setupFlashcardEntry(flashcards[prop]);
			displayDiv.appendChild(newDiv);
		}
	}
	if(displayDiv.innerHTML == ""){ var errorDiv = document.createElement("div"); errorDiv.className = "MessageDiv"; errorDiv.appendChild(document.createTextNode("Sorry, that word hasn't been saved to your flashcards yet.")); displayDiv.appendChild(errorDiv); } } function startSearchForFlashcard(){ var searchBox = document.getElementById("searchBox");
	var searchWord = searchBox.value;
	searchBox.value = "";
	var message = prepMessage("GetAllFlashcards");
	passMessage(message, searchForFlashcard.bind(searchWord, searchWord));
}

//Creates div with search box and search button for flashcard
//search and returns the div
function setupSearchForFlashcard(){
	var searchDiv = document.getElementById("SearchDiv");
	searchDiv.innerHTML = "";
	document.getElementById("FCDisplayDiv").innerHTML="";
	var searchBox = document.createElement("input");
	searchBox.type = "text";
	searchBox.id = "searchBox";
	searchBox.style.marginRight = "10px";
	
	searchDiv.appendChild(searchBox);
	
	var searchButton = document.createElement("button");
	searchButton.innerHTML = "Search for word";
	searchButton.addEventListener("click", startSearchForFlashcard);
	
	searchDiv.appendChild(searchButton);
}

//Adds event listeners to the drop down menu so clicking on any of them opens
//the corresponding page.
document.getElementById("flashcardMenu").addEventListener("change", function(){
	if(this.value == "Browse") getAllFlashcards();
	if(this.value == "Search") setupSearchForFlashcard();
	if(this.value == "Review") {
		chrome.tabs.create({active: true, url: 'flashcardReview.html'});
	}
});
