//For getting words from background, use:
//{"sender" : "LookUpBar", "recipient": "background", "msg" : ["Next" | "Prev" | "Word"]}
//For replying from background, use:
//{"sender" : "background", "recipient": "LookUpBar", "empty" : [true | false], ["nextResults" | "prevResults" | "lookUpResults"] : words[]}

var displayStatus = false;

//Used for debugging
function printToConsole(somethingToBePrinted){
	//console.log(somethingToBePrinted);
}

//Basic BarDiv object.
//This object is the base for Look^Up as it appears to
//and is used by the user.
function BarDiv() {
	
	//Flag for whether or not links are turned on
	this.linksOn = true;

	//Flag for whether or not the Look^Up bar is displayed
	this.displayOn = true;

	//Array to hold all results for current search
	this.currentLookUp = [];
	
	//An arry holding keystrokes from user. 
	//(Cleared every few keystrokes)
	this.keyLog = [];	
}

//Basic array for passing messgaes to the background script
//See below for message formatting
//Include callback function that will be using response
BarDiv.prototype.passMessage = function(message, callback){
	chrome.runtime.sendMessage(message, function(res){
		callback(res);
	});
}

//Function to prepare messages to be sent to background
//msg should indicate what process background will be performing
//(See background.js for available operations if necessary)
//Include content holds payload, such search terms, etc.
BarDiv.prototype.prepMessage = function(msg, content){
	if(content == undefined) content = "";
	var message = {
		"sender" : "LookUpBar",
		"recipient" : "background",
		"msg" : msg,
		"content" : content
	}
	
	return message;
}

//This funciton is called when the user wants to search
//for some selected text.
//
BarDiv.prototype.getSelectedText = function(){
	var selected = "";

	selected = document.getSelection().toString();
	printToConsole(selected);
	return selected;
}

//This function will find all <a> links in the document,
//copy their content, and add divs with their content.
//The <a> links are then hidden by setting their display
//values to none.
BarDiv.prototype.killAllLinks = function(){
	var allLinks = document.getElementsByTagName("a");
	var dLinks = document.getElementsByClassName("dClass");
	if(dLinks.length == 0){
		for(var i = 0; i < allLinks.length; i++){
			var dLink = document.createElement("div");
			dLink.innerHTML = allLinks[i].innerHTML;
			dLink.className = "dClass";
			allLinks[i].parentNode.insertBefore(dLink, allLinks[i]);
		}
	}
	dLinks = document.getElementsByClassName("dClass");
	for(var i = 0; i < dLinks.length; i++){
		dLinks[i].style.display = "initial";
	}
	for(var z = 0; z < allLinks.length; z++){
		allLinks[z].style.display = "none";
		allLinks
	}
}

//This function is called when a new item is added to 
//the user's flashcards. Note: newWord is a word object.
BarDiv.prototype.addToFlashcards = function(newWord){
	
	var message = this.prepMessage("AddFlashcard", newWord);
	
	//printToConsole is used primarily for debugging
	//and doesn't actually print anything in final version.
	this.passMessage(message, printToConsole);
}


//Creates and returns a button for adding a specific
//search result to the flashcards.
//x is a word object.
BarDiv.prototype.newFlashcardButton = function(x, bar){
	var newButton = document.createElement("input");
	newButton.style.all = "initial";
	newButton.type = "button";
	newButton.class = "FCButton";
	newButton.style.border = "black solid 1px";
	newButton.style.backgroundColor = "white";
	newButton.style.borderRadius = "3px";
	newButton.style.cursor = "pointer";
	newButton.style.paddingLeft = "3px";
	newButton.style.paddingRight = "3px";
	newButton.title = " Add to flashcards ";
	newButton.value = " Add to flashcards ";
	newButton.addEventListener('click',function(){
		bar.addToFlashcards(x);
	});
	
	newButton.addEventListener('mouseover',function(){
		newButton.style.backgroundColor="#6666ff";
	});
	
	newButton.addEventListener('mouseout',function(){
		newButton.style.backgroundColor="white";
	});
	
	return newButton;
}

//This function is used to load search results in the
//look up bar. wordArray is an array of word objecs,
//as you would probably expect.
BarDiv.prototype.loadLookUp = function(wordArray, emptyFlag){
	var counter = 0;
	var resDiv = document.getElementById("searchResultsDiv");
	
	resDiv.innerHTML = "";
	if(emptyFlag){
		var curDiv = document.createElement("div")
		curDiv.style.all = "initial";
		curDiv.appendChild(document.createTextNode("Sorry, the word you searched for was not found in the dictionary."));
		curDiv.style.fontSize = "20px";
		curDiv.style.fontWeight = "bold";
		resDiv.appendChild(curDiv)
	}
	for(prop in wordArray){
		var wordSpan = document.createElement("span");
		wordSpan.innerHTML = "<b>Word: </b>" + wordArray[prop]["word"];
		wordSpan.style.paddingRight = "15px";
		var readingSpan = document.createElement("span")
		readingSpan.innerHTML = "<b>Reading: </b>" + wordArray[prop]["reading"];
		var defSpan = document.createElement("span");
		defSpan.innerHTML = "<b>Definition: </b>" + wordArray[prop]["definition"] + " ";
		
		var flashcardButton = this.newFlashcardButton(wordArray[prop], this);
		
		var curDiv = document.createElement("div");
		curDiv.style.all = "initial";
		curDiv.appendChild(wordSpan);
		curDiv.appendChild(readingSpan);
		curDiv.appendChild(document.createElement("br"));
		curDiv.appendChild(defSpan);
		curDiv.appendChild(document.createElement("br"));
		curDiv.appendChild(flashcardButton);
		curDiv.style.width = "100%";
		curDiv.style.display = "inline-block";
		curDiv.style.paddingBottom = "2px";
		
		if(counter % 2 == 1){
			curDiv.style.backgroundColor = "#e6f2ff";
		}
		counter++;
		
		resDiv.appendChild(curDiv);
	}
}

//Intermediary function, called after looking up a word
//or when showing look up bar with word from look up history.
BarDiv.prototype.prepareResults = function(results){
	printToConsole(results);
	if(!results.empty) this.loadLookUp(results["lookUpResults"], false);
	else this.loadLookUp(results["lookUpResults"], true)
}

//Function for processing results from getting history.
//If the results are empty (i.e. there are no words or the
//user has reached either end of the history list), then
//the background will flash red.
//Otherwise, the search results saved in the history
//will be loaded in the look up bar.
BarDiv.prototype.prepareGetHistory = function(results){
	if(results["empty"]){
		var resDiv = document.getElementById("searchResultsDiv");
		resDiv.style.backgroundColor = "#ff4d4d";
		setTimeout(function(){resDiv.style.backgroundColor = "white"}, 100);
	}
	else {
		this.updateSearchWord(results.SearchWord);
		this.loadLookUp(results["History"]);
	}
}


//Sends a message to the background script requesting next word
//in history and then return results to prepareGetHistory 
BarDiv.prototype.getNextLookUp = function(){
	var message = this.prepMessage("Next");
	this.passMessage(message, this.prepareGetHistory.bind(this));
}

//Sends a message to the background script requesting previous word
//in history and then return results to prepareGetHistory 
BarDiv.prototype.getPrevLookUp = function(){
	var message = this.prepMessage("Prev");
	this.passMessage(message, this.prepareGetHistory.bind(this));
}

//First checks if the bar exists on the page (some pages
//destroy the bar while loading, such as feedly) and recreates
//it if necessary.
//The text selected by the user is then gotten and passed to
//the background for a search.
BarDiv.prototype.startSearch = function(){
	if(document.getElementById("lookUpDiv") == undefined){
		barReset();
	}
	if(!this.displayOn) this.showDiv();
	
	var curText = this.getSelectedText();
	if(curText.length > 0) this.wordSearch(curText);
	else{
		var resDiv = document.getElementById("searchResultsDiv");

		resDiv.innerHTML = "";
		resDiv.innerHTML = "No text selected.<br>Please note that text in iframes (such as embedded tweets) cannot be selected for security reasons."
	}
}

//Updates the left side of the look up bar with the search term.
//Uses innerHTML to clear the div.
BarDiv.prototype.updateSearchWord = function(newSearchWord){
	var searchWordSpan = document.getElementById("searchWordSpan");
	searchWordSpan.innerHTML = "";
	searchWordSpan.appendChild(document.createTextNode(newSearchWord));
}

//Function to initiate a word search.
BarDiv.prototype.wordSearch = function(inputWord){
	this.updateSearchWord(inputWord);
	var message = this.prepMessage("Word", inputWord);
	printToConsole(message);
	var searchResult = this.passMessage(message, this.prepareResults.bind(this));
}

//As the name implies, this function hides the look up bar.
//It also passes a message to the background, setting the
//display flag in the background script.
BarDiv.prototype.hideDiv = function(){
	this.mainDiv.style.display = "none";
	this.displayOn = false;
	var message = this.prepMessage("DisplayStatus", ["Set",false]);
	this.passMessage(message, printToConsole);
}

//Shows the div, like hideDiv.
//Passes a message to background script, setting the 
//display flag.
BarDiv.prototype.showDiv = function(){
	this.mainDiv.style.display = "";
	this.displayOn = true;
	var message = this.prepMessage("DisplayStatus", ["Set",true]);
	this.passMessage(message, printToConsole);
}

//Some sites will destory the look up bar object
//in the process of loading (such as feedly).
//This function just creates a new look up bar object.
function barReset(){
	var barDiv = new BarDiv;
	barDiv.setup();
}

BarDiv.prototype.onKeyUp = function(e){
	curKey = e.keyCode;
	if(curKey == 17) this.keyLog.length = 0;
}

BarDiv.prototype.onKeyDown = function(e){
	curKey = e.keyCode;
	
	//Clean up
	if(this.keyLog.length > 2) this.keyLog.length = 0;
	
	//Push control to array
	if(curKey == 17) this.keyLog.push(17);
	
	//Lookup selection and display BarDiv
	if(this.keyLog[0] == 17 && curKey == 81){
		this.startSearch();
		this.keyLog.length = 0;
	}
		
	//Move through History
	if(this.keyLog[0] == 17 && curKey == 39){
		this.getNextLookUp();
	}
	if(this.keyLog[0] == 17 && curKey == 37){
		this.getPrevLookUp();
	}
	
	//Hide BarDiv
	if(curKey == 27){
		if(this.displayOn) this.hideDiv();
	}
}

//Redisplays all <a> links on a page by chaning link
//display values to "" and hiding the replacement divs.
BarDiv.prototype.resurrectAllLinks = function(){
	var dLinks = document.getElementsByClassName("dClass");
	var allLinks = document.getElementsByTagName("a");
	for(var z = 0; z < dLinks.length; z++){
		dLinks[z].style.display = "none";
	}
	for(var z = 0; z < allLinks.length; z++){
		allLinks[z].style.display = "";
	}	
}

//Changes the link icon based on the status of link display
BarDiv.prototype.linkStatus = function(){
	var linksOnImage = chrome.runtime.getURL('linkTiny.png');
	var linksOffImage = chrome.runtime.getURL('nolinkTiny.png');
	if(this.linksOn){
		this.linksOn = false;
		var linkDiv = document.getElementById("linkDiv");
		linkDiv.style.backgroundImage = "url(" + linksOffImage + ")";
		this.killAllLinks();
	} else {
		this.linksOn = true;
		var linkDiv = document.getElementById("linkDiv");
		linkDiv.style.backgroundImage = "url(" + linksOnImage + ")";
		this.resurrectAllLinks();
	}
}

//Function to prepare main div and return it for diplay
BarDiv.prototype.setupMainDiv = function(){
	var mainDiv = document.createElement("div");
	mainDiv.id = "lookUpDiv";
	mainDiv.style.all = "initial";
	mainDiv.style.width = "75%";
	mainDiv.style.height = "100px";
	mainDiv.style.position = "fixed";
	mainDiv.style.display = "inline-block";
	mainDiv.style.top = "100px";
	mainDiv.style.left = "12.5%";
	mainDiv.style.right = "12.5%";
	mainDiv.style.backgroundColor = "#e6f2ff";
	mainDiv.style.zIndex = "999999999";
	mainDiv.style.fontFamily = "Arial";
	mainDiv.style.border = "blue solid 2px";
	return mainDiv;
}

//Track movement of mouse when dragging bar for respositioning.
BarDiv.prototype.trackMove = function (evt){
	var lookUpDiv = document.getElementById("lookUpDiv");
	lookUpDiv.style.left = evt.clientX - 2 + "px";
	lookUpDiv.style.top = evt.clientY - 2 + "px";
}

//When the div is clicked, mousemove and mouseup event 
//listeners are added.
BarDiv.prototype.startMove = function(evt){
	document.body.addEventListener('mousemove', this.trackMove);
	document.body.addEventListener('mouseup', this.endMove.bind(this));
}

//On mouse up, the even listeners added above are removed.
BarDiv.prototype.endMove = function(evt){
	document.body.removeEventListener('mouseup', this.endMove.bind(this));
	document.body.removeEventListener('mousemove', this.trackMove);
}

//Create div for click and drag corner in look up bar
//and return the div to be added.
BarDiv.prototype.setupMoveDiv = function(){
	var innerDiv = document.createElement("div");
	innerDiv.id = "inMyDiv";
	innerDiv.style.width = "10px";
	innerDiv.style.height = "10px";
	var corner = chrome.runtime.getURL('corner.png');
	innerDiv.style.backgroundImage = "url("+corner+")";
	innerDiv.style.position = "absolute";
	innerDiv.style.top = "0";
	innerDiv.style.left = "0";
	innerDiv.addEventListener('mousedown', this.startMove.bind(this));
	innerDiv.style.cursor = "move";
	innerDiv.title = "Click and drag to move the Look^Up bar";
	return innerDiv;
}

//Create div for icon to hide/show links and return it.
BarDiv.prototype.setupLinkDiv = function(){
	var linkDiv = document.createElement("div");
	linkDiv.id = "linkDiv";
	linkDiv.style.position = "absolute";
	linkDiv.style.width = "30px";
	linkDiv.style.height = "7px";
	linkDiv.style.right = "2px";
	linkDiv.style.top = "2px";
	var linkOnImage = chrome.runtime.getURL('linkTiny.png');
	linkDiv.style.backgroundImage = "url(" + linkOnImage + ")";
	linkDiv.style.backgroundRepeat = "no-repeat";
	linkDiv.addEventListener('click', this.linkStatus.bind(this));
	linkDiv.style.cursor = "pointer";
	linkDiv.title = "Click to turn all links on/off";
	return linkDiv;
}

//Create div to hold search term and return it
BarDiv.prototype.setupSearchWord = function(){
	var searchWordDiv = document.createElement("div");
	searchWordDiv.style.all = "initial";
	searchWordDiv.id = "searchWordDiv";
	searchWordDiv.style.width = "23%";
	searchWordDiv.style.height = "56px";
	searchWordDiv.style.textAlign = "center";
	searchWordDiv.style.bottom = "5px";
	searchWordDiv.style.left = "5px";
	searchWordDiv.style.fontSize = "24px";
	searchWordDiv.style.paddingTop = "24px";
	var searchWordSpan = document.createElement("span");
	searchWordSpan.appendChild(document.createTextNode("Look^Up"));
	searchWordSpan.id = "searchWordSpan";
	searchWordDiv.appendChild(searchWordSpan);
	searchWordDiv.style.border = "blue 1px solid";
	searchWordDiv.style.position = "absolute";
	searchWordDiv.style.display = "inline-block";
	searchWordDiv.style.backgroundColor = "white";
	searchWordDiv.style.overflow = "hidden";
		
	return searchWordDiv;
}

//Create div to hold serach results and return it
BarDiv.prototype.setupSearchResults = function (){
	var searchResultsDiv = document.createElement("div");
	searchResultsDiv.style.all = "initial";
	searchResultsDiv.id = "searchResultsDiv";
	searchResultsDiv.style.width = "calc(100% - 23% - 26px";
	searchResultsDiv.style.height = "74px";
	searchResultsDiv.style.bottom = "5px";
	searchResultsDiv.style.right = "5px";
	searchResultsDiv.style.fontSize = "18px";
	searchResultsDiv.style.border = "blue 1px solid";
	searchResultsDiv.style.position = "absolute";
	searchResultsDiv.style.display = "inline-block";
	searchResultsDiv.style.paddingLeft = "3px";
	searchResultsDiv.style.paddingRight = "3px";
	searchResultsDiv.style.paddingTop = "3px";
	searchResultsDiv.style.paddingBottom = "3px";
	searchResultsDiv.style.backgroundColor = "white";
	searchResultsDiv.style.overflowX = "hidden";
		
	return searchResultsDiv;
}

//Set display status and hide/display look up bar.
//Also calls prepareResults, which will prepare history 
//results to be dipslayed in searchResultsDiv in case we
//are loading the bar after clicking a link and navigating
//to a new webpage.
BarDiv.prototype.setDisplayStatus = function(curStatus){
	printToConsole(curStatus);
	this.prepareResults(curStatus);
	if(!curStatus.empty && curStatus.SearchWord)
		this.updateSearchWord(curStatus.SearchWord);
	if(curStatus["DisplayStatus"]) this.showDiv();
	else this.hideDiv();
}

//Pepares message to be passed to background and sends
//to background, with callback function (setDisplayStatus)
BarDiv.prototype.checkDisplayStatus = function(){
	var message = this.prepMessage("DisplayStatus", "Get");
	var displayStatus = this.passMessage(message, this.setDisplayStatus.bind(this));
}

//Sets up the basics for a look up bar and appends
//the div to the current document
BarDiv.prototype.setup = function(){
	this.mainDiv = this.setupMainDiv();
	this.moveDiv = this.setupMoveDiv();
	this.linkDiv = this.setupLinkDiv();
	this.searchWordDiv = this.setupSearchWord();
	this.searchResultsDiv = this.setupSearchResults();
	this.mainDiv.style.display = "none";
		
	this.mainDiv.appendChild(this.moveDiv);
	this.mainDiv.appendChild(this.linkDiv);
	this.mainDiv.appendChild(this.searchWordDiv);
	this.mainDiv.appendChild(this.searchResultsDiv);
	this.mainDiv.style.overflow = "hidden";
	
	this.checkDisplayStatus();
	
	document.body.appendChild(this.mainDiv);
}

//create new BarDiv object
var barDiv = new BarDiv;
barDiv.setup();

//Add keydown/keyup event listeners
document.addEventListener("keydown", barDiv.onKeyDown.bind(barDiv));
document.addEventListener("keyup", barDiv.onKeyUp.bind(barDiv));

//Set up message listener in case of messages received from background
chrome.runtime.onMessage.addListener(function(res, sender, resSend){
	if(res["sender"] == "background" && res["recipient"] == "LookUpBar"){
		if(res["msg"]=="LookupSelectedText"){
			barDiv.startSearch();
		}
	}
});
